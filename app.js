const { json, send } = require('micro')
const micro = require('micro')
const { router, get } = require('microrouter')
const html = require('./html')

function hello (req, res) {
  send(res, 200, `${html.html}`)
}

let routes = cors(router(
  get('/', hello)
))

if (process.env.NODE_ENV === 'development') {
  module.exports = routes
} else {
  const server = micro(routes)
  server.listen(appPort, () => {
    console.log(`Server listening on port ${appPort}`)
  })
}